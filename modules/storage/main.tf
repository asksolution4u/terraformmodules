
data "azurerm_resource_group" "storage" {
  name = var.resource_group_name
}

data "azurerm_virtual_network" "network" {
  name                = var.virtual_network_name
  resource_group_name = data.azurerm_resource_group.storage.name
}
data "azurerm_subnet" "subnet" {
  name                 = var.subnet_name
  virtual_network_name = data.azurerm_virtual_network.network.name
  resource_group_name  = data.azurerm_resource_group.storage.name
}
data "azurerm_client_config" "current" {}

data "azurerm_key_vault" "key" {
  name                = var.kvault_name
  resource_group_name = var.kvault_rg
}
data "azurerm_key_vault_key" "key" {
  name         = var.keyname_storage
  key_vault_id = data.azurerm_key_vault.key.id
}
resource "azurerm_storage_account" "storageacc" {
  name                     = var.storage_name
  resource_group_name      = data.azurerm_resource_group.storage.name
  location                 = var.storage_location != null ? var.storage_location : data.azurerm_resource_group.storage.location
  account_tier             = var.account_tier
  account_replication_type = var.replication_type
  account_kind             = var.storage_kind
  allow_nested_items_to_be_public = "false"
  is_hns_enabled           = "true"
  identity {
    type = "SystemAssigned"
  }
  network_rules {
    default_action             = "Allow"
  }
  blob_properties {
    delete_retention_policy {
      days = 10
    }
    container_delete_retention_policy {
      days = 10
    }
  }
}
resource "azurerm_private_endpoint" "blobend" {
  name                = "${var.blob_end}-${azurerm_storage_account.storageacc.name}"
  location            = var.storage_location != null ? var.storage_location : data.azurerm_resource_group.storage.location
  resource_group_name = data.azurerm_resource_group.storage.name
  subnet_id           = data.azurerm_subnet.subnet.id

  private_service_connection {
    name                           = var.connection_name
    is_manual_connection           = false
    private_connection_resource_id = azurerm_storage_account.storageacc.id
    subresource_names              = ["blob"]
  }
  }

resource "azurerm_private_endpoint" "dfsend" {
  name                = "${var.dfs_end}-${azurerm_storage_account.storageacc.name}"
  location            = var.storage_location != null ? var.storage_location : data.azurerm_resource_group.storage.location
  resource_group_name = data.azurerm_resource_group.storage.name
  subnet_id           = data.azurerm_subnet.subnet.id

  private_service_connection {
    name                           = var.connection_dfs_name
    is_manual_connection           = false
    private_connection_resource_id = azurerm_storage_account.storageacc.id
    subresource_names              = ["dfs"]
  }
}
resource "azurerm_key_vault_access_policy" "storage" {
  key_vault_id = data.azurerm_key_vault.key.id
  tenant_id    = data.azurerm_client_config.current.tenant_id
  object_id    = azurerm_storage_account.storageacc.identity[0].principal_id


 key_permissions = [

      "Backup",
      "Create",
      "Decrypt",
      "Delete",
      "Encrypt",
      "Get",
      "Import",
      "List",
      "Purge",
      "Recover",
      "Restore",
      "Sign",
      "UnwrapKey",
      "Update",
      "Verify",
      "WrapKey",
       "Rotate",
      "GetRotationPolicy",
      "SetRotationPolicy"
    ]     
    }

resource "azurerm_storage_account_customer_managed_key" "storage" {
  storage_account_id = azurerm_storage_account.storageacc.id
  key_vault_id       = data.azurerm_key_vault.key.id
  key_name           = data.azurerm_key_vault_key.key.name
  depends_on = [azurerm_key_vault_access_policy.storage]
}