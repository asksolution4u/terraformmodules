variable "storage_name" {
  description = "Name of the vnet to create"
  type        = string
}
variable "resource_group_name" {
  description = "Name of the resource group to be imported."
  type        = string
}
variable "virtual_network_name" {
  description = "Name of the vnet to be imported."
  type        = string
}
variable "subnet_name" {
  description = "Name of the vnet to be imported."
  type        = string
}
variable "storage_location" {
  description = "The location of the storage to create. Defaults to the location of the resource group."
  type        = string
  default     = null
}

variable "account_tier" {
  description = "account tier to create"
  type        = string
  default     = "Standard"
}

variable "replication_type" {
  description = "replication type of storage"
  type        = string
  default     = "LRS"
}

variable "storage_kind" {
  description = "account kind to create"
  type        = string
  default     = "StorageV2"
}

variable "blob_end" {
  description = "endpoint name"
  type        = string
  default     = "endforblob"
}

variable "connection_name" {
  type        = string
  default     = "endconnection"
}
  
variable "dfs_end" {
  description = "endpoint name"
  type        = string
  default     = "endfordfs"
}

variable "connection_dfs_name" {
  type        = string
  default     = "enddfsconnection"
}
  
variable "kvault_name" {
  type        = string
  default     = "name of key vault"
}

 variable "kvault_rg" {
  type        = string
  default     = "rg of key vault"
}
 variable "keyname_storage" {
  type        = string
  default     = "name of key"
}
variable "file_system_name" {
  type        = string
  default     = "container for synapse"
}