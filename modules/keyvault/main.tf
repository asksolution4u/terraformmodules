#module key vault
data "azurerm_resource_group" "kvault" {
  name = var.resource_group_name
}

data "azurerm_client_config" "current" {}


resource "azurerm_key_vault" "kvault" {
  name                        = var.kvault_name
  location                    = data.azurerm_resource_group.kvault.location
  resource_group_name         = data.azurerm_resource_group.kvault.name
  enabled_for_disk_encryption = var.disk_encryption
  tenant_id                   = data.azurerm_client_config.current.tenant_id
  soft_delete_retention_days  = var.soft_retention_days
  purge_protection_enabled = "true"

  sku_name = var.sku

  
network_acls {
    default_action = "Allow"
    bypass         = "AzureServices"
  }

}
resource "azurerm_key_vault_access_policy" "current" {
    key_vault_id = azurerm_key_vault.kvault.id
    tenant_id = data.azurerm_client_config.current.tenant_id
    object_id = data.azurerm_client_config.current.object_id

    key_permissions = [

      "Backup",
      "Create",
      "Decrypt",
      "Delete",
      "Encrypt",
      "Get",
      "Import",
      "List",
      "Purge",
      "Recover",
      "Restore",
      "Sign",
      "UnwrapKey",
      "Update",
      "Verify",
      "WrapKey",
       "Rotate",
      "GetRotationPolicy",
      "SetRotationPolicy"
    ]     
    secret_permissions = [
      "Set",
      "Get",
      "Delete",
      "Purge",
      "Recover",


    ]

    storage_permissions = [
      "Get",
       "List",
      "Set",
    
  
    ]
  }

resource "azurerm_key_vault_access_policy" "app_reg" {
      key_vault_id = azurerm_key_vault.kvault.id
  tenant_id    = data.azurerm_client_config.current.tenant_id
  object_id    = var.service_principal_object_id

    key_permissions    = ["Get", "Create", "List", "Restore", "Recover", "UnwrapKey", "WrapKey", "Purge", "Encrypt", "Decrypt", "Sign", "Verify","Delete","Rotate","GetRotationPolicy","SetRotationPolicy"]


  secret_permissions = [
    "Get",
    "List",
    "Set",
    "Delete",

  ]
}

resource "azurerm_key_vault_key" "key_storage" {
  name         = var.keyname_storage
  key_vault_id = azurerm_key_vault.kvault.id
  key_type     = var.keytype
  key_size     = var.keysize
  key_opts     = ["decrypt", "encrypt", "sign", "unwrapKey", "verify", "wrapKey"]
 depends_on = [azurerm_key_vault.kvault,azurerm_key_vault_access_policy.app_reg,azurerm_key_vault_access_policy.current]
}

resource "azurerm_key_vault_key" "key_synapse"{
  name         = var.keyname_synapse
  key_vault_id = azurerm_key_vault.kvault.id
  key_type     = var.keytype
  key_size     = var.keysize
  key_opts     = ["decrypt", "encrypt", "sign", "unwrapKey", "verify", "wrapKey"]
 depends_on = [azurerm_key_vault.kvault,azurerm_key_vault_access_policy.app_reg,azurerm_key_vault_access_policy.current]

}

