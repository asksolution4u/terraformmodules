variable "resource_group_name" {
  description = "Name of the resource group to be imported."
  type        = string
}

variable "kvault_name" {
  description = "Name of the keyvault."
  type        = string
}
variable "disk_encryption" {
  description = "disk encryption "
  type        = bool
}

variable "soft_retention_days" {
  description = "days for soft delete"
  type        = string
}

variable "sku" {
  description = "sku for key vault"
  type        = string
}
  
  variable "keyname_storage" {
  description = "endpoint name for key vault"
  type        = string
}
variable "keyname_synapse" {
  description = "endpoint name for key vault"
  type        = string
}
  
  variable "keysize" {
  description = "endpoint name for key vault"
  type        = string
}

variable "keytype" {
  description = "endpoint name for key vault"
  type        = string
}

variable "service_principal_object_id"{
  description = "object id of your service principal(app_registration) used for terraform"
}