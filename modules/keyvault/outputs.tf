output "keyvault_id" {
  value = azurerm_key_vault.kvault.id
}

output "keyvault_name" {
  value = azurerm_key_vault.kvault.name
}