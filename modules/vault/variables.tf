variable "resource_group_name" {
  description = "The name of the resource group."
  type        = string
}

variable "azurerm_key_vault_name" {
  description = "The name of the Azure Key Vault instance."
  type        = string
}

variable "location" {
  description = "The location of the resource group."
  type        = string
}

variable "azurerm_key_vault_sku_name" {
  description = "The SKU of the Azure Key Vault instance."
  type        = string
}

variable "enabled_for_disk_encryption" {
  description = "Boolean flag indicating whether or not the key vault is enabled for disk encryption."
  type        = bool
}

variable "purge_protection_enabled" {
  description = "Boolean flag indicating whether or not the key vault has purge protection enabled."
  type        = bool
}

variable "key_type" {
  description = "The type of the key vault key."
  type        = string
}

variable "key_size" {
  description = "The size of the key vault key."
  type        = number
}

variable "azurerm_key_vault_key_name" {
  description = "Name of key."
  type        = string
}

