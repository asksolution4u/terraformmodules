data "azurerm_resource_group" "example" {
  name = var.resource_group_name
}

data "azurerm_client_config" "current" {}



resource "azurerm_key_vault" "example" {
  name                = var.azurerm_key_vault_name
   location            =var.location
  resource_group_name = data.azurerm_resource_group.example.name
  sku_name            = var.azurerm_key_vault_sku_name 
   enabled_for_disk_encryption =var.enabled_for_disk_encryption 
   purge_protection_enabled    = var.purge_protection_enabled 
  tenant_id                   = data.azurerm_client_config.current.tenant_id
}

resource "azurerm_key_vault_key" "example_key" {
  name         = var.azurerm_key_vault_key_name 
  key_vault_id = azurerm_key_vault.example.id
  key_type     = var.key_type 
  key_size     = var.key_size 

  depends_on = [
    azurerm_key_vault_access_policy.example-user
  ]

  key_opts = [
    "decrypt",
    "encrypt",
    "sign",
    "unwrapKey",
    "verify",
    "wrapKey",
  ]
}

resource "azurerm_key_vault_access_policy" "example-user" {
  key_vault_id = azurerm_key_vault.example.id

  tenant_id = data.azurerm_client_config.current.tenant_id
  object_id = data.azurerm_client_config.current.object_id

  key_permissions = [
    "Create",
    "Delete",
    "Get",
    "Purge",
    "UnwrapKey",
    "WrapKey",
    "Recover",
    "Update",
    "List",
    "Decrypt",
    "Sign",
    "GetRotationPolicy",
  ]
}

