data "azurerm_resource_group" "example" {
  name = var.resource_group_name
}


resource "azurerm_virtual_network" "vnet" {
  for_each = var.vnet_configs
  name                = each.key
  location            = var.location
  resource_group_name = data.azurerm_resource_group.example.name
  address_space       = each.value.address_space
  
}

resource "azurerm_subnet" "subnet" {
  for_each = var.subnets
  name                  = each.key
  resource_group_name   = data.azurerm_resource_group.example.name
  virtual_network_name = each.value.vnet_name
  address_prefixes      = each.value.address_space
}

resource "azurerm_nat_gateway" "nat_gateway" {
  for_each = var.nat_gateways
  name                = each.key
  location            = var.location
  resource_group_name = data.azurerm_resource_group.example.name
  sku_name                = each.value.sku_name
}

resource "azurerm_subnet_nat_gateway_association" "nat_association" {
 for_each = {
    for subnet_name, subnet_config in var.subnets : subnet_name => {
      subnet_id = azurerm_subnet.subnet[subnet_name].id
      nat_gateway_id = azurerm_nat_gateway.nat_gateway[lookup(subnet_config, "nat_gateway_name")].id
    }
  }

  subnet_id      = each.value.subnet_id
  nat_gateway_id = each.value.nat_gateway_id
}

resource "azurerm_public_ip" "nat_public_ip" {
  for_each = var.nat_gateways
  name                = "${each.key}-public-ip"
  location            = var.location
  resource_group_name = data.azurerm_resource_group.example.name
  allocation_method = each.value.allocation_method
  #allocation_method   = "Static"
  sku = each.value.sku_name
  #sku                 = "Standard"
}

resource "azurerm_nat_gateway_public_ip_association" "example" {
  for_each = { for k, v in var.nat_gateways : k => v.public_ip_enabled if v.public_ip_enabled }
  nat_gateway_id       = azurerm_nat_gateway.nat_gateway[each.key].id
  public_ip_address_id = azurerm_public_ip.nat_public_ip[each.key].id
}

