variable "resource_group_name" {
  type = string
  default = ""
}

variable "location" {
  type = string
  default = "East US"
  description = "Location of resources"
}


variable "vnet_configs" {
  type = map(object({
    address_space = list(string)
    
  }))
  default = {}

}

variable "subnets" {
  type = map(object({
    vnet_name = string
    address_space = list(string)
    nat_gateway_name = string
  }))
  default ={}
}


variable "nat_gateways" {
  type = map(object({
    sku_name = string
    allocation_method   = string
    public_ip_enabled = bool
  }))
  default = {}
}

