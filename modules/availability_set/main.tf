
data "azurerm_resource_group" "example" {
  name = var.resource_group_name
}


resource "azurerm_availability_set" "example" {
  name                = var.availability_set_name
  resource_group_name = data.azurerm_resource_group.example.name 
  location            = var.location 
   tags                         = var.tags

}


