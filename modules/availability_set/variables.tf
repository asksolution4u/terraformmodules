variable "availability_set_name"{
    type = string
    default = ""
    description = "availability set name"
}

variable "resource_group_name" {
  description = "The name of the resource group."
  type        = string
  default = ""
}


variable "location" {
  description = "The location of the resource group."
  type        = string
  default = ""
}

variable "tags" {
   description = "Map of the tags to use for the resources that are deployed"
   type        = map(string)
   default = {}
}
