data "azurerm_resource_group" "synapse" {
  name = var.endpoints_rg
}

data "azurerm_virtual_network" "synapse" {
  name                = var.virtual_network_name
  resource_group_name = var.vnet_rg
}
data "azurerm_subnet" "synapse" {
  name                 = var.subnet_name
  virtual_network_name = data.azurerm_virtual_network.synapse.name
  resource_group_name  = var.vnet_rg
}
data "azurerm_synapse_workspace" "synapse" {
  name                = var.synapse_name
  resource_group_name = var.synapse_rg_name
}


resource "azurerm_private_endpoint" "synapse-sql" {

  name                = "${var.sql_endpoint}-${data.azurerm_synapse_workspace.synapse.name}"
  resource_group_name = data.azurerm_resource_group.synapse.name
  location            = data.azurerm_resource_group.synapse.location
  subnet_id           = data.azurerm_subnet.synapse.id
private_service_connection {
    name                           = var.sql_connection
    private_connection_resource_id = data.azurerm_synapse_workspace.synapse.id
    subresource_names              = [ "Sql" ]
    is_manual_connection           = false
  }
  

}

resource "azurerm_private_endpoint" "synapse-sql_demand" {

  name                = "${var.sql_on_demand_endpoint}-${data.azurerm_synapse_workspace.synapse.name}"
  resource_group_name = data.azurerm_resource_group.synapse.name
  location            = data.azurerm_resource_group.synapse.location
  subnet_id           = data.azurerm_subnet.synapse.id
private_service_connection {
    name                           = var.sql_on_demand_connection
    private_connection_resource_id = data.azurerm_synapse_workspace.synapse.id
    subresource_names              = [ "SqlOnDemand" ]
    is_manual_connection           = false
  }
  

}

resource "azurerm_private_endpoint" "synapse-dev" {

  name                = "${var.dev_endpoint}--${data.azurerm_synapse_workspace.synapse.name}"
  resource_group_name = data.azurerm_resource_group.synapse.name
  location            = data.azurerm_resource_group.synapse.location
  subnet_id           = data.azurerm_subnet.synapse.id
private_service_connection {
    name                           = var.dev_connection
    private_connection_resource_id = data.azurerm_synapse_workspace.synapse.id
    subresource_names              = [ "Dev" ]
    is_manual_connection           = false
  }
  
}