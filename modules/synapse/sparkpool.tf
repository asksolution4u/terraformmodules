resource "azurerm_synapse_spark_pool" "sparkpool" {
  name                 = var.spark_pool_name
  synapse_workspace_id = azurerm_synapse_workspace.synapse.id
  node_size_family     = var.spark_node_size
  node_size            = var.node_size
  cache_size           = var.spark_cache_size

  auto_scale {
    max_node_count = var.max_node_count_spark
    min_node_count = var.min_node_count_spark
  }

  auto_pause {
    delay_in_minutes = var.autopause_delay
  }
depends_on = [azurerm_synapse_workspace_key.synapse]
}