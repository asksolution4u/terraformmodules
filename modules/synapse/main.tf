data "azurerm_resource_group" "synapse" {
  name = var.resource_group_name
}

data "azurerm_storage_account" "synapse" {
  name                = var.storage_account_name
  resource_group_name =  data.azurerm_resource_group.synapse.name
}
data "azurerm_key_vault" "example" {
  name                = var.sql_creds_vault
  resource_group_name = data.azurerm_resource_group.synapse.name
}

data "azurerm_key_vault_secret" "sql_user" {
  name         = "sql-admin"
  key_vault_id = data.azurerm_key_vault.example.id
}

data "azurerm_key_vault_secret" "sql_password" {
  name         = "sql-password"
  key_vault_id = data.azurerm_key_vault.example.id
}

resource "azurerm_synapse_workspace" "synapse" {
  name                                 = var.workspace_name
  resource_group_name                  = data.azurerm_resource_group.synapse.name
  location                             = data.azurerm_resource_group.synapse.location
  storage_data_lake_gen2_filesystem_id = var.data_lake_gen2_filesystem
  sql_administrator_login              = data.azurerm_key_vault_secret.sql_user.value
  sql_administrator_login_password     =data.azurerm_key_vault_secret.sql_password.value
  managed_virtual_network_enabled      = true
  public_network_access_enabled        = true
  data_exfiltration_protection_enabled = true
  managed_resource_group_name          = var.managed_resource_group
   customer_managed_key {
    key_versionless_id = data.azurerm_key_vault_key.synapse.versionless_id
    key_name           = var.synapse_key
  }

  identity {
    type = var.identity_type
  }
}
resource "azurerm_synapse_workspace_aad_admin" "synapse" {
  synapse_workspace_id = azurerm_synapse_workspace.synapse.id
  login                = var.ad_admin
  object_id            = var.admin_object
  tenant_id            = var.admin_tenant

  depends_on           = [azurerm_synapse_workspace_key.synapse]
}

resource "azurerm_role_assignment" "adls-synapse-managed-identity" {
  scope                = data.azurerm_storage_account.synapse.id
  role_definition_name = "Storage Blob Data Contributor"
  principal_id         = azurerm_synapse_workspace.synapse.identity[0].principal_id

  depends_on = [ azurerm_synapse_workspace.synapse ]
}

resource "azurerm_role_assignment" "adls-synapse-managed-identity-reader" {
  scope                = data.azurerm_storage_account.synapse.id
  role_definition_name = "Storage Blob Data Reader"
  principal_id         = azurerm_synapse_workspace.synapse.identity[0].principal_id

  depends_on = [ azurerm_synapse_workspace.synapse ]
}



data "azurerm_virtual_network" "network" {
  name                = var.virtual_network_name
  resource_group_name = data.azurerm_resource_group.synapse.name
}
data "azurerm_subnet" "subnet" {
  name                 = var.subnet_name
  virtual_network_name = data.azurerm_virtual_network.network.name
  resource_group_name  = data.azurerm_resource_group.synapse.name
}


resource "azurerm_storage_account_network_rules" "example" {
  storage_account_id =data.azurerm_storage_account.synapse.id
 default_action = "Deny"
    bypass =[ "AzureServices"]
    virtual_network_subnet_ids = [
     data.azurerm_subnet.subnet.id
    ]
    private_link_access {
      endpoint_tenant_id   = var.admin_tenant
      endpoint_resource_id = azurerm_synapse_workspace.synapse.id
    }
}
