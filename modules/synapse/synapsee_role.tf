resource "azurerm_synapse_firewall_rule" "synapse" {
  name                 = "AllowAll"
  synapse_workspace_id = azurerm_synapse_workspace.synapse.id
  start_ip_address     = "0.0.0.0"
  end_ip_address       = "255.255.255.255"
  depends_on = [azurerm_synapse_workspace.synapse]
}
resource "azurerm_synapse_firewall_rule" "azure_services" {
  name                 = "AllowAllWindowsAzureIps"
  synapse_workspace_id = azurerm_synapse_workspace.synapse.id
  start_ip_address     = "0.0.0.0"
  end_ip_address       = "0.0.0.0"
  depends_on = [azurerm_synapse_workspace.synapse]
}
resource "azurerm_synapse_role_assignment" "synapse" {
  synapse_workspace_id = azurerm_synapse_workspace.synapse.id
  role_name            = "Synapse Administrator"
  principal_id         = var.synapse_admin_object_id
  depends_on = [azurerm_synapse_workspace.synapse, azurerm_synapse_firewall_rule.synapse]
}
