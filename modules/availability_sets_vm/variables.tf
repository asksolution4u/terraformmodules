variable "availability_set_id"{
    type = string
    default = ""
    description = "availability set id"
}

variable "security_rules" {
  type = list(object({
    name                       = string
    priority                   = number
    description = optional(string)
    destination_address_prefixes = optional(string)
    destination_application_security_group_ids = optional(string)
    destination_port_ranges = optional(string)
    source_address_prefixes = optional(string)
    source_application_security_group_ids = optional(string)
    source_port_ranges = optional(string)
    direction                  = string
    access                     = string
    protocol                   = string
    source_port_range          = string
    destination_port_range     = string
    source_address_prefix      = string
    destination_address_prefix = string
  }))
}
  

  variable "subnet_name" {
  description = "The name of the subnet."
  type        = string
  default = ""
}

variable "virtual_network_name" {
  description = "The name of the virtual network."
  type        = string
  default = ""
}

variable "resource_group_name" {
  description = "The name of the resource group."
  type        = string
  default = ""
}


variable "location" {
  description = "The location of the resource group."
  type        = string
  default = ""
}





variable "azurerm_public_ip_name" {
  description = "The name of the public IP address."
  type        = string
  default = ""
}

variable "azurerm_public_ip_allocation_method" {
  description = "The allocation method of the public IP address."
  type        = string
  default = ""
}

variable "azurerm_network_interface" {
  description = "The name of the network interface."
  type        = string
  default = ""
}

variable "ip_configuration_name" {
  description = "The name of the IP configuration."
  type        = string
  default = ""
}

variable "private_ip_address_allocation" {
  description = "The private IP address allocation method."
  type        = string
  default = ""
}


variable "azurerm_network_security_group_name" {
  description = "The name of the network security group."
  type        = string
  default = ""
}

variable "azurerm_linux_virtual_machine_name" {
  description = "The name of the Linux virtual machine."
  type        = string
  default = ""
}

variable "azurerm_linux_virtual_machine_size" {
  description = "The size of the Linux virtual machine."
  type        = string
  default = ""
}

variable "admin_username" {
  description = "The username of the Linux virtual machine admin."
  type        = string
  default = ""
}

variable "admin_password" {
  description = "The password of the Linux virtual machine admin."
  type        = string
  default = ""
}

variable "source_image_reference_sku" {
  description = "The SKU of the source image reference."
  type        = string
  default = ""
}

variable "os_disk_storage_account_type" {
  description = "The storage account type of the os disk."
  type        = string
  default = ""
}
variable "os_disk_caching" {
  description = "The caching type  of the os disk."
  type        = string
  default = ""
}


variable "init_script_name"{

   description = "name of script"
  type        = string
  default = ""

}
