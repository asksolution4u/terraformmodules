data "azurerm_client_config" "current" {}

data "azurerm_key_vault" "synapse" {
  name                = var.keyvault_name
  resource_group_name = var.kvault_rg
}

data "azurerm_key_vault_key" "synapse" {
  name         = var.synapse_key
  key_vault_id = data.azurerm_key_vault.synapse.id
 
}

resource "azurerm_key_vault_access_policy" "synapse" {
  key_vault_id = data.azurerm_key_vault.synapse.id
  tenant_id    = data.azurerm_client_config.current.tenant_id
  object_id    = azurerm_synapse_workspace.synapse.identity[0].principal_id

  key_permissions    = ["Get", "Create", "List", "Restore", "Recover", "UnwrapKey", "WrapKey", "Purge", "Encrypt", "Decrypt", "Sign", "Verify"]
  secret_permissions = ["Get"]
    depends_on                          = [azurerm_synapse_workspace.synapse]

}

resource "azurerm_synapse_workspace_key" "synapse" {
  customer_managed_key_versionless_id = data.azurerm_key_vault_key.synapse.versionless_id
  synapse_workspace_id                = azurerm_synapse_workspace.synapse.id
  active                              = true
  customer_managed_key_name           = var.synapse_key
  depends_on                          = [azurerm_key_vault_access_policy.synapse]
}