variable "resource_group_name" {
  description = "Name of the resource group to be imported."
  type        = string
}



variable "managed_resource_group" {
  description = "Name of the resource group to be imported."
  type        = string
  default = null
}

variable "storage_account_name" {
  description = "ADLS needed for filesystem"
  type        = string
}

variable "data_lake_gen2_filesystem" {
  description = "Name of the filesystem to be created for synapse."
  type        = string
}
variable "workspace_name" {
  description = "Unique name of the synapse workspace."
  type        = string
}

variable "sql_admin" {
  description = "Name of the admin."
  type        = string
}

variable "sql_password" {
  description = "Password for synapse."
  type        = string
}

variable "identity_type" {
  description = "User or System assinged identity."
  type        = string
}

variable "sql_pool_name" {
  description = "Name for sql pool"
  type        = string
}

variable "sql_pool_sku" {
  description = "Sku for sql pool."
  type        = string
}

variable "create_mode" {
  description = "Create mode for sql pool"
  type        = string
}

variable "kvault_rg" {
  description = "name for the vnet resource group"
  type        = string
}

 variable "keyvault_name" {
  description = "name of the keyvault"
  type        = string
}

variable "synapse_key" {
  description = "name of the key"
  type        = string
}
 
 variable "ad_admin" {
  description = "name of the azure active directory admin"
  type        = string
}
 
variable "admin_object" {
  description = "object_id for ad admin"
  type        = string
}

variable "admin_tenant" {
  description = "tenant id for ad admin"
  type        = string
}

variable "node_size"{
  description = "node type for spark pool"
}

variable "spark_pool_name"{
  description = "name for spark pool"
}

variable "spark_node_size"{
  description = "spark node size family"
}

variable "spark_cache_size"{
  description = "spark cache size"
}

variable "max_node_count_spark"{
  description = "max node count for spark pool"
}

variable "min_node_count_spark"{
  description = "min node count for spark pool"
}

variable "autopause_delay"{
  description = "spark autopause delay"
}

variable "synapse_admin_object_id"{
  description = "object id to be synapse administrator"
}

variable "synapse_name" {
  description = "name for the synapse workspace for which private endpoint have to be created"
  type        = string
}

variable "synapse_rg_name" {
  description = "name for the resource group for synapse_workspace"
  type        = string
}
variable "endpoints_rg" {
  description = "name for the resource group for locating endpoints for synapse_workspace"
  type        = string
}

variable "virtual_network_name" {
  description = "name for the virtual network for private endpoint"
  type        = string
}

variable "vnet_rg" {
  description = "name for the vnet resource group"
  type        = string
}

variable "subnet_name" {
  description = "name for the subnet for private endpoint"
  type        = string
}

variable "sql_endpoint" {
  description = "name for the endpoint for sql"
  type        = string
}

variable "sql_connection" {
  description = "name for the endpoint connection for sql"
  type        = string
}
variable "sql_on_demand_endpoint" {
  description = "name for the endpoint for sql on demand"
  type        = string
}

variable "sql_on_demand_connection" {
  description = "name for the endpoint connection for sql on demand"
  type        = string
}
variable "dev_endpoint" {
  description = "name for the endpoint for dev"
  type        = string
}

variable "dev_connection" {
  description = "name for the endpoint connection for dev"
  type        = string
}