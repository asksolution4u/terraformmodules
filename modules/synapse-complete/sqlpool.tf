resource "azurerm_synapse_sql_pool" "synapse" {
  name                 = var.sql_pool_name
  synapse_workspace_id = azurerm_synapse_workspace.synapse.id
  sku_name             = var.sql_pool_sku
  create_mode          = var.create_mode

    depends_on = [  azurerm_synapse_workspace.synapse , azurerm_synapse_workspace_key.synapse]

}