variable "resource_group_name" {
   description = "Name of the resource group in which the resources will be created"
   default     = ""
}

variable "location" {
   default = "eastus"
   description = "Location where resources will be created"
}


variable "init_script_name"{

  description = "name of script"
  type        = string
  default = ""

}

variable "tags" {
   description = "Map of the tags to use for the resources that are deployed"
   type        = map(string)
   default = {}
}

variable "application_port" {
   description = "Port that you want to expose to the external load balancer"
   default     = 80
}

variable "lb_name" {
   description = "name of load balancer"
   default     = ""
}

variable "ip_allocation_method" {
   description = "IP allocation method"
   default     = ""
}

variable "subnet_name" {
   description = "subnet name"
   default     = ""
}

variable "virtual_network_name" {
   description = "Virtual network namer"
   default     = ""
}

variable "lb_public_ip_name" {
   description = "IP name for load balancer"
   default     = ""
}

variable "lb_rule_name" {
   description = "rule name for load balancer"
   default     = ""
}



variable "admin_user" {
   description = "User name to use as the admin account on the VMs that will be part of the VM scale set"
   default     = "azureuser"
}

variable "admin_password" {
   description = "Default password for admin account"
   default = "Admin@1234567"
}


variable "sku_name" {
  description = "The name of the virtual machine scale set SKU to use."
  default     = ""
}

variable "sku_tier" {
  description = "The tier of the virtual machine scale set SKU to use."
  default     = ""
}

variable "capacity" {
  description = "The number of virtual machines in the scale set."
  default     = null
}

variable "image_publisher" {
  description = "The publisher of the virtual machine image."
  default     = ""
}

variable "image_offer" {
  description = "The offer of the virtual machine image."
  default     = ""
}

variable "image_sku" {
  description = "The SKU of the virtual machine image."
  default     = ""
}

variable "image_version" {
  description = "The version of the virtual machine image."
  default     = ""
  
}

variable "os_disk_name" {
  description = "The name of the OS disk."
  default     = ""
}


variable "os_caching_type" {
  description = "The caching type for the OS disk."
  default     = ""
}

variable "os_create_option" {
  description = "The creation option for the OS disk."
  default     = ""
}

variable "managed_disk_type" {
  description = "The type of managed disk to use."
  default     = ""
}

variable "data_disk_caching_type" {
  description = "The caching type for the data disk."
  default     = ""
}

variable "data_disk_create_option" {
  description = "The creation option for the data disk."
  default     = ""
}

variable "data_size_gb" {
  description = "The size of the data disk in GB."
  default     = 10
}

variable "lun" {
  description = "The Logical Unit Number (LUN) of the data disk."
  default     = 0
}

variable "os_profile_computer_name" {
  description = "The computer name prefix for the virtual machines in the scale set."
  default     = ""
}

variable "scale_set_name" {
  description = "scale set name "
  default     = ""
}

variable "azurerm_monitor_autoscale_setting_name" {
  description = "monitor setting name"
  default     = ""
}

variable "disable_password_authentication" {
  description = "Whether or not to disable password authentication for SSH on the virtual machines."
  default     = false
}

variable "network_profile_name" {
  description = "The name of the network profile for the virtual machine scale set."
  default     = ""
}

variable "upgrade_policy_mode" {
  description = "The upgrade policy mode for the virtual machine scale set."
  default     = ""
}
