variable "subnet_name" {
  description = "The name of the subnet."
  type        = string
}

variable "virtual_network_name" {
  description = "The name of the virtual network."
  type        = string
}

variable "resource_group_name" {
  description = "The name of the resource group."
  type        = string
}

variable "private_service_connection_name" {
  description = "The name of the private service conn."
  type        = string
}

variable "private_endpoint_name" {
  description = "The name of the private endpoint."
  type        = string
}


variable "location" {
  description = "The location of the resource group."
  type        = string
}

variable "subresource_names" {
  description = "subresources name"
  type        = list
}

variable "private_connection_resource_id" {
  description = "The resource id to which private connection will be applied"
  type        = string
}
