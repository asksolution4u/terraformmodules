data "azurerm_resource_group" "example" {
  name = var.resource_group_name
}


data "azurerm_subnet" "example" {
  name                 = var.subnet_name
  virtual_network_name = var.virtual_network_name
 resource_group_name = data.azurerm_resource_group.example.name
  
}


resource "azurerm_private_endpoint" "example" {
  name                = var.private_endpoint_name 
  location            =var.location 
  resource_group_name = data.azurerm_resource_group.example.name
   subnet_id                     = data.azurerm_subnet.example.id
   
  
  private_service_connection {
    name                           = var.private_service_connection_name 
    private_connection_resource_id =var.private_connection_resource_id 
    is_manual_connection           = false
    subresource_names              = var.subresource_names
  }
}