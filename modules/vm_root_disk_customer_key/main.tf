data "azurerm_resource_group" "example" {
  name = var.resource_group_name
}


data "azurerm_client_config" "current" {}

data "azurerm_subnet" "example" {
  name                 = var.subnet_name
  virtual_network_name = var.virtual_network_name
  resource_group_name  = var.resource_group_name
}


resource "azurerm_public_ip" "public_ip" {
  name                = var.azurerm_public_ip_name 
  location            =var.location 
  resource_group_name = data.azurerm_resource_group.example.name 
  allocation_method   = var.azurerm_public_ip_allocation_method
  sku =var.azurerm_public_ip_sku_name  
}

resource "azurerm_network_interface" "test" {
  name                = var.azurerm_network_interface 
  resource_group_name = data.azurerm_resource_group.example.name 
  location            = var.location 

  ip_configuration {
    name                          = var.ip_configuration_name 
    subnet_id                     = data.azurerm_subnet.example.id
    private_ip_address_allocation = var.private_ip_address_allocation 
    public_ip_address_id = azurerm_public_ip.public_ip.id
  }
}

resource "azurerm_network_security_group" "nsg" {
  name                = var.azurerm_network_security_group_name 
  location            = var.location 
  resource_group_name = data.azurerm_resource_group.example.name 
  security_rule = [
    for rule in var.security_rules : {
      name                       = rule.name
      priority                   = rule.priority
       description = rule.description
   destination_address_prefixes = rule.destination_address_prefixes != null ? toset(rule.destination_address_prefixes) : null
    destination_application_security_group_ids = rule.destination_application_security_group_ids != null ? toset(rule.destination_application_security_group_ids) : null
    destination_port_ranges     = rule.destination_port_ranges != null ? toset(rule.destination_port_ranges) : null
    source_address_prefixes     = rule.source_address_prefixes != null ? toset(rule.source_address_prefixes) : null
    source_application_security_group_ids = rule.source_application_security_group_ids != null ? toset(rule.source_application_security_group_ids) : null
    source_port_ranges          = rule.source_port_ranges != null ? toset(rule.source_port_ranges) : null
      direction                  = rule.direction
      access                     = rule.access
      protocol                   = rule.protocol
      source_port_range          = rule.source_port_range
      destination_port_range     = rule.destination_port_range
      source_address_prefix      = rule.source_address_prefix
      destination_address_prefix = rule.destination_address_prefix
    }
  ]
  
  }

resource "azurerm_subnet_network_security_group_association" "example" {
  subnet_id                 = data.azurerm_subnet.example.id
  network_security_group_id = azurerm_network_security_group.nsg.id
}

resource "azurerm_network_interface_security_group_association" "association" {
  network_interface_id      = azurerm_network_interface.test.id
  network_security_group_id = azurerm_network_security_group.nsg.id
}






resource "azurerm_key_vault" "example" {
  name                = var.azurerm_key_vault_name
   location            =var.location
  resource_group_name = data.azurerm_resource_group.example.name
  sku_name            = var.azurerm_key_vault_sku_name 
   enabled_for_disk_encryption =var.enabled_for_disk_encryption 
   purge_protection_enabled    = var.purge_protection_enabled 
  tenant_id                   = data.azurerm_client_config.current.tenant_id
}

resource "azurerm_key_vault_key" "example_key" {
  name         = var.azurerm_key_vault_key_name 
  key_vault_id = azurerm_key_vault.example.id
  key_type     = var.key_type 
  key_size     = var.key_size 

  depends_on = [
    azurerm_key_vault_access_policy.example-user
  ]

  key_opts = [
    "decrypt",
    "encrypt",
    "sign",
    "unwrapKey",
    "verify",
    "wrapKey",
  ]
}

resource "azurerm_key_vault_access_policy" "example-disk" {
  key_vault_id = azurerm_key_vault.example.id

  tenant_id = azurerm_disk_encryption_set.example.identity.0.tenant_id
  object_id = azurerm_disk_encryption_set.example.identity.0.principal_id

  key_permissions = [
    "Create",
    "Delete",
    "Get",
    "Purge",
    "UnwrapKey",
    "WrapKey",
    "Recover",
    "Update",
    "List",
    "Decrypt",
    "Sign",
  ]
}


resource "azurerm_disk_encryption_set" "example" {
  name                = var.azurerm_disk_encryption_set_name
   location            = var.location
  resource_group_name = data.azurerm_resource_group.example.name
  encryption_type = var.azurerm_disk_encryption_set_type 
  key_vault_key_id        = azurerm_key_vault_key.example_key.id
  identity {
    type = var.azurerm_disk_encryption_set_identy_type
  }
}


resource "azurerm_key_vault_access_policy" "example-user" {
  key_vault_id = azurerm_key_vault.example.id

  tenant_id = data.azurerm_client_config.current.tenant_id
  object_id = data.azurerm_client_config.current.object_id

  key_permissions = [
    "Create",
    "Delete",
    "Get",
    "Purge",
    "UnwrapKey",
    "WrapKey",
    "Recover",
    "Update",
    "List",
    "Decrypt",
    "Sign",
    "GetRotationPolicy",
  ]
}



resource "azurerm_linux_virtual_machine" "example" {
   name                            = var.azurerm_linux_virtual_machine_name
  resource_group_name             = data.azurerm_resource_group.example.name 
  location                        = var.location
  custom_data =filebase64(init_script_name)
   size                            = var.azurerm_linux_virtual_machine_size 
  admin_username                  = var.admin_username
  disable_password_authentication = false
  admin_password = var.admin_password  
   network_interface_ids = [
    azurerm_network_interface.test.id,
  ]
  

    os_disk {
    storage_account_type = var.os_disk_storage_account_type 
    caching              = var.os_disk_caching
    disk_size_gb = var.os_disk_size 
    disk_encryption_set_id = azurerm_disk_encryption_set.example.id
  } 
  

  source_image_reference {
   publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = var.source_image_reference_sku
    version   = "latest"
  }
}

