data "azurerm_resource_group" "example" {
  name = var.resource_group_name
}




resource "azurerm_web_application_firewall_policy" "example" {
  name                =  var.azurerm_web_application_firewall_policy_name 
  location            =var.location 
  resource_group_name = data.azurerm_resource_group.example.name
  dynamic "custom_rules" {
    for_each = var.custom_rules
    content {
      name      = custom_rules.value.name
      priority  = custom_rules.value.priority
      rule_type = custom_rules.value.rule_type

      dynamic "match_conditions" {
        for_each = custom_rules.value.match_conditions
        content {
          dynamic "match_variables" {
            for_each = match_conditions.value.match_variables
            content {
              variable_name = match_variables.value.variable_name
              selector      = match_variables.value.selector
            }
          }

          operator           = match_conditions.value.operator
          negation_condition = match_conditions.value.negation_condition
          match_values       = match_conditions.value.match_values
        }
      }

      action = custom_rules.value.action
    }
  }

  /*
  dynamic "custom_rules" {
     for_each = var.custom_rules
    content {
    
    name      = custom_rules.value.name
    priority  = custom_rules.value.priority
    rule_type = custom_rules.value.rule_type

    dynamic "match_conditions" {
      for_each = each.value.match_conditions
      content {
        dynamic "match_variables" {
          for_each = match_conditions.value.match_variables
          content {
            variable_name = match_variables.value.variable_name

            dynamic "selector" {
              for_each = match_variables.value.selector != null ? [match_variables.value.selector] : []
              content {
                selector = selector.value
              }
            }
          }
        }

        operator           = match_conditions.value.operator
        negation_condition = match_conditions.value.negation_condition
        match_values       = match_conditions.value.match_values
      }
    }
    }

    action = each.value.action
  }

  */
  dynamic "policy_settings" {
    for_each = var.policy_settings
    content {
      enabled                     = policy_settings.value.enabled
      mode                        = policy_settings.value.mode
      request_body_check          = policy_settings.value.request_body_check
      file_upload_limit_in_mb     = policy_settings.value.file_upload_limit_in_mb
      max_request_body_size_in_kb = policy_settings.value.max_request_body_size_in_kb
    }
  }

  dynamic "managed_rules" {
    for_each = var.managed_rules
    content {
      dynamic "exclusion" {
        for_each = managed_rules.value.exclusion
        content {
          match_variable          = exclusion.value.match_variable
          selector                = exclusion.value.selector
          selector_match_operator = exclusion.value.selector_match_operator
        }
      }

      dynamic "managed_rule_set" {
        for_each = managed_rules.value.managed_rule_set
        content {
          type    = managed_rule_set.value.type
          version = managed_rule_set.value.version

          dynamic "rule_group_override" {
            for_each = managed_rule_set.value.rule_group_override
            content {
              rule_group_name = rule_group_override.value.rule_group_name

              dynamic "rule" {
                for_each = rule_group_override.value.rule
                content {
                  id      = rule.value.id
                  enabled = rule.value.enabled
                  action  = rule.value.action
                }
              }
            }
          }
        }
      }
    }
  }
}
