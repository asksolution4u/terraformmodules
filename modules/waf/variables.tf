variable "custom_rules" {
  type = list(object({
    name        = string
    priority    = number
    rule_type   = string
    match_conditions = list(object({
      match_variables = list(object({
        variable_name = string
        selector      = optional(string)
      }))
      operator           = string
      negation_condition = bool
      match_values       = list(string)
    }))
    action      = string
  }))
}

variable "policy_settings" {
  type = list(object({
    enabled                     = bool
    mode                        = string
    request_body_check          = bool
    file_upload_limit_in_mb     = number
    max_request_body_size_in_kb = number
  }))
}

variable "managed_rules" {
  type = list(object({
    exclusion = list(object({
      match_variable          = string
      selector                = string
      selector_match_operator = string
    }))
    managed_rule_set = list(object({
      type    = string
      version = string
      rule_group_override = list(object({
        rule_group_name = string
        rule = list(object({
          id      = string
          enabled = bool
          action  = string
        }))
      }))
    }))
  }))
}


variable "azurerm_web_application_firewall_policy_name"{
  type = string
  default = ""
  description = "name of waf policy"
}

variable "resource_group_name" {
  description = "The name of the resource group."
  type        = string
}


variable "location" {
  description = "The location of the resource group."
  type        = string
}
