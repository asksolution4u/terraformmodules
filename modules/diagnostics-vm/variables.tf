variable "diagnostics_settings_name" {
    type = string
    description = "(optional) diagnostics setting name"
    default = ""
}

variable "logs" {
    default = []
 }

variable "metrics" { 
    default = []
}


variable "sku"{
    type = string
    default = ""
}

variable "retention_days"{
    type = number
    default = 30
}

variable "log_analytics_workspace_id"{
    type = string
    default = ""
}

variable "location"{
    type = string
    default = ""
}

variable "virtual_machine_id"{
    type = string
    default = ""
}

variable "resource_group_name"{
    type = string
    default = ""
}