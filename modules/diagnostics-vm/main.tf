data "azurerm_monitor_diagnostic_categories" "this" {
  resource_id = data.azurerm_virtual_machine.example.id
}


data "azurerm_resource_group" "example" {
  name = var.resource_group_name
}

data "azurerm_virtual_machine" "example" {
  name                = var.virtual_machine_id
  resource_group_name = data.azurerm_resource_group.example.name
}

resource "azurerm_log_analytics_workspace" "example" {
  name                = var.log_analytics_workspace_id 
  location            = var.location
  resource_group_name = data.azurerm_resource_group.example.name
  sku                 = var.sku 
  retention_in_days   = var.retention_days
}

resource "azurerm_monitor_diagnostic_setting" "this" {
  name                       = var.diagnostics_settings_name
  target_resource_id         = data.azurerm_virtual_machine.example.id
  log_analytics_workspace_id = azurerm_log_analytics_workspace.example.id
  
  dynamic "log" {
    for_each = toset(data.azurerm_monitor_diagnostic_categories.this.logs)

    content {
      category = log.value
      enabled  = contains(var.logs,log.value) 

      retention_policy {
          enabled = contains(var.logs,log.value)
          days    = contains(var.logs,log.value) == true ? var.retention_days :0
      }
    }
  }


  dynamic "metric" {
    for_each = toset(data.azurerm_monitor_diagnostic_categories.this.metrics)

    content {
      category = metric.value
      enabled  = contains(var.metrics,metric.value) 

      retention_policy {
        enabled = contains(var.metrics,metric.value)
        days    = contains(var.metrics,metric.value) == true ? var.retention_days :0
      }
    }
  }

  lifecycle {
    ignore_changes = [ metric ]
  }
}
